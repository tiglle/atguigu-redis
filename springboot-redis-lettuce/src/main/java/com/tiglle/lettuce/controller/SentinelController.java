package com.tiglle.lettuce.controller;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class SentinelController {

    @Resource
    private RedisTemplate redisTemplate;

    /**
     * 哨兵模式测试
     * @return
     */
    @GetMapping("sentinel")
    public String sentinel(){

        redisTemplate.opsForValue().set("tiglle1","tiglle");

        return "success";
    }


}
