package com.tiglle.lettuce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootRedisLettuce {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootRedisLettuce.class,args);
    }
}
